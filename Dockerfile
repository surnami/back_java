FROM openjdk:8-jre-alpine

LABEL name="maven/jdk8/alpine container"
LABEL version="1.0"
LABEL architecture="x86_64"

WORKDIR /home

COPY . .

ARG TEST_A
ENV MYSQL_PWD=TEST_A

EXPOSE 8008
EXPOSE 22

CMD ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "out/security-service.jar"]
