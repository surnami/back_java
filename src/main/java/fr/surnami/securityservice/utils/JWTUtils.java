package fr.surnami.securityservice.utils;

import fr.surnami.securityservice.service.UserDetailsImpl;
import io.jsonwebtoken.*;
import org.slf4j.*;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JWTUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(JWTUtils.class);
    public static final String PREFIX = "Bearer ";
    public static final String SECRET = System.getenv("KEY_SEC_1");
    public static final String AUTH_HEADER = "Authorization";
    public static final double EXPIRE_DELAY = 120000;
    public static final double REFRESH_DELAY = 120000000;
    public static final Date EXPIRY_DATE = new Date((long) (System.currentTimeMillis()+JWTUtils.EXPIRE_DELAY));
    public static final Date LONGEVITY = new Date((long) (System.currentTimeMillis()+JWTUtils.REFRESH_DELAY));

    public static String generateJwtToken(Authentication authentication) {

        UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();

        return Jwts.builder()
                .setSubject((userPrincipal.getUsername()))
                .setIssuedAt(new Date())
                .setExpiration(EXPIRY_DATE)
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }

    public static String extractUserNameFromToken(String token) {
        return Jwts.parser().setSigningKey(JWTUtils.SECRET).parseClaimsJws(token).getBody().getSubject();
    }

    public static boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(JWTUtils.SECRET).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            LOGGER.error("Invalid JWT signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            LOGGER.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            LOGGER.error("JWT token is expired: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            LOGGER.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            LOGGER.error("JWT claims string is empty: {}", e.getMessage());
        }
        return false;
    }
}
