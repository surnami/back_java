package fr.surnami.securityservice.forms;

import lombok.Data;

@Data
public class RoleUserForm {
    private String username;
    private String roleName;
}
