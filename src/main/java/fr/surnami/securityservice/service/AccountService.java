package fr.surnami.securityservice.service;

import fr.surnami.securityservice.entities.*;
import fr.surnami.securityservice.repositories.*;
import fr.surnami.securityservice.repositories.AppUserRepository;
import lombok.Data;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;


@Service
@Transactional
@Data
public class AccountService implements AccountServiceFactory {
    private final AppUserRepository appUserRepository;
    private final AppRoleRepository appRoleRepository;
    private final PasswordEncoder passwordEncoder;

    public AccountService(AppUserRepository appUserRepository,
                          AppRoleRepository appRoleRepository,
                          PasswordEncoder passwordEncoder) {
        this.appUserRepository = appUserRepository;
        this.appRoleRepository = appRoleRepository;
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public AppUser addNewUser(AppUser appUser) {
        String password = appUser.getPassword();
        appUser.setPassword(passwordEncoder.encode(password));
        appUserRepository.save(appUser);
        return appUser;
    }

    @Override
    public AppRole addNewRole(AppRole appRole) {
        appRoleRepository.save(appRole);
        return appRole;
    }

    @Override
    public void addRoleToUser(String username, String role) {
        AppUser pickedUser = appUserRepository.findByUsername(username);
        AppRole pickedRole = appRoleRepository.findByName(role);
        pickedUser.getAppRoles().add(pickedRole);
    }

    @Override
    public AppUser searchUser(String username) {
        AppUser foundUser = appUserRepository.findByUsername(username);
        return foundUser;
    }

    @Override
    public List<AppUser> listUsers() {
        return appUserRepository.findAll();
    }

    @Override
    public AppUser editUser(AppUser editedUser) {
        AppUser replacement = appUserRepository.findByUsername(editedUser.getUsername());
        replacement.setPassword(editedUser.getPassword());
        replacement.setAppRoles(editedUser.getAppRoles());
        appUserRepository.save(replacement);
        return replacement;
    }

    @Override
    public List<AppRole> listRoles() {
        return appRoleRepository.findAll();
    }

    @Override
    public AppRole searchRole(String name) {
        return appRoleRepository.findByName(name);
    }

    @Override
    public AppRole editRole(AppRole editedRole) {
        AppRole replacement = appRoleRepository.findByName(editedRole.getName());
        replacement.setName(editedRole.getName());
        appRoleRepository.save(replacement);
        return replacement;
    }

    @Override
    public boolean isUsernameUnique(String username) {
        return appUserRepository.existsByUsername(username);
    }

    @Override
    public boolean isEmailUnique(String email) {
        return appUserRepository.existsByEmail(email);
    }


}
