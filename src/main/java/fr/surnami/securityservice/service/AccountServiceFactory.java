package fr.surnami.securityservice.service;

import fr.surnami.securityservice.entities.AppRole;
import fr.surnami.securityservice.entities.AppUser;

import java.util.List;

public interface AccountServiceFactory {
    AppUser addNewUser(AppUser appUser);
    AppRole addNewRole(AppRole appRole);

    void addRoleToUser(String username, String role);
    AppUser searchUser(String username);
    List<AppUser> listUsers();
    AppUser editUser(AppUser editedUser);
    List<AppRole> listRoles();
    AppRole searchRole(String role);
    AppRole editRole(AppRole editedRole);
    boolean isUsernameUnique(String username);
    boolean isEmailUnique(String email);
}
