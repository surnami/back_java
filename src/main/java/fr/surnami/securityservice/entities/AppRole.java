package fr.surnami.securityservice.entities;

import lombok.*;
import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor @AllArgsConstructor
public class AppRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String name;
}
