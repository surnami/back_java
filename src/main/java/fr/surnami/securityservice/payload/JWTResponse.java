package fr.surnami.securityservice.payload;

import fr.surnami.securityservice.utils.JWTUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data @AllArgsConstructor @NoArgsConstructor
public class JWTResponse {
    private String token;
    private String type = JWTUtils.PREFIX;
    private Long id;
    private String username;
    private String email;
    private List<String> roles;

    public JWTResponse(String accessToken, Long id, String username, String email, List<String> roles) {
        this.token = accessToken;
        this.id = id;
        this.username = username;
        this.email = email;
        this.roles = roles;
    }
}
