package fr.surnami.securityservice;

import fr.surnami.securityservice.entities.*;
import fr.surnami.securityservice.service.AccountServiceFactory;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@ComponentScan(basePackages = "fr.surnami.securityservice.*")
@EnableJpaRepositories(basePackages = "fr.surnami.securityservice.repositories")
@EntityScan(basePackages = "fr.surnami.securityservice.entities")
public class SecurityServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityServiceApplication.class, args);
    }


    @Bean
    CommandLineRunner start( AccountServiceFactory accountServiceFactory) {
        return args -> {
            /*
            accountServiceFactory.addNewRole(new AppRole(null, "USER"));
            accountServiceFactory.addNewRole(new AppRole(null, "ADMIN"));
            accountServiceFactory.addNewRole(new AppRole(null, "COMMUNITY MANAGER"));
            accountServiceFactory.addNewRole(new AppRole(null, "INTERN"));
            accountServiceFactory.addNewRole(new AppRole(null, "OPERATOR"));

            accountServiceFactory.addNewUser(new AppUser(null, "user1", "user1@ibm.com", "123456", null));

            accountServiceFactory.addRoleToUser("user1", "USER");
            accountServiceFactory.addRoleToUser("user1", "ADMIN");
            */
        };
    }
}
